<#
.SYNOPSIS
    Downloads and installs Connectship Warehouse using the provided instructions
.DESCRIPTION
    Long description
.EXAMPLE
    PS C:\> <example usage>
    Explanation of what the example does
.NOTES
    General notes
#>
[CmdletBinding(SupportsShouldProcess)]
[Diagnostics.CodeAnalysis.SuppressMessageAttribute('PSAvoidUsingPlainTextForPassword', 'MembersAreaPassword',
        Justification = 'There is no other clean way to handle this')]
param (
    # Hostname of the on-premise connectship server. For example, connectship-dev.central.com
    [Parameter(Mandatory=$true)]
    [String]
    $Server,
    # Username used to retrieve post-install updates directly from Connectship.
    [Parameter(Mandatory=$true)]
    [String]
    $MembersAreaUser,
    # Password used to retrieve post-install updates directly from Connectship.
    [Parameter(Mandatory=$true)]
    [String]
    $MembersAreaPassword,
    # Indicates whether updates should be installed immediately post-update 
    [Parameter(Mandatory=$false)]
    [Boolean]
    $PostInstallUpdates = $true,
    # Indicates whether work files should be deleted after the installation completes
    [Parameter(Mandatory=$false)]
    [Boolean]
    $CleanupTempFiles = $true,
    # Path on the connectship server hosting the dll & installer
    [Parameter(Mandatory=$false)]
    [String]
    $DownloadUrlPath = "csw/install",
    # Name of the installer
    [Parameter(Mandatory=$false)]
    [String]
    $CSWInstaller = "CSWShippingInstall.exe",
    # Name of the DLL file
    [Parameter(Mandatory=$false)]
    [String]
    $CSWDLLName = "cswutils.dll",
    # Local directory containing the work files
    [Parameter(Mandatory=$false)]
    [String]
    $TempPath = "C:\Temp"
)

function Get-CSWInstaller {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory=$true)]
        [String]
        $Server,
        [Parameter(Mandatory=$true)]
        [String]
        $DownloadUrlPath,
        [Parameter(Mandatory=$true)]
        [String]
        $TempPath,
        [Parameter(Mandatory=$true)]
        [String]
        $CSWInstaller,
        [Parameter(Mandatory=$true)]
        [System.Net.WebClient]
        $Client
    )

    $uri = "http://${Server}/${DownloadUrlPath}/${CSWInstaller}"
    $target = "${TempPath}\${CSWInstaller}"
    if($PSCmdlet.ShouldProcess($uri)) {
        $Client.DownloadFile($uri,$target)
    }
    Write-Verbose "'${uri}' downloaded to '${target}'"
}

function Install-CSWDll {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory=$true)]
        [String]
        $Server,
        [Parameter(Mandatory=$true)]
        [String]
        $DownloadUrlPath,
        [Parameter(Mandatory=$true)]
        [String]
        $TempPath,
        [Parameter(Mandatory=$true)]
        [String]
        $CSWDLLName,
        [Parameter(Mandatory=$true)]
        [Boolean]
        $CleanupTempFiles,
        [Parameter(Mandatory=$true)]
        [System.Net.WebClient]
        $Client
    )

    # Download the DLL
    $uri = "http://${Server}/${DownloadUrlPath}/${CSWDLLName}"
    $dllPath = "${TempPath}\${CSWDLLName}"
    if($PSCmdlet.ShouldProcess($uri)) {
        $Client.DownloadFile($uri,$dllPath)
    }
    Write-Verbose "'${uri}' downloaded to '${dllPath}'"

    # Download the checksum
    $uri = "http://${Server}/${DownloadUrlPath}/${CSWDLLName}_checksum.json"
    $checksumPath = "${TempPath}\${CSWDLLName}_checksum.json"
    if($PSCmdlet.ShouldProcess($uri)) {
        $Client.DownloadFile($uri,$checksumPath)
    }
    Write-Verbose "'${uri}' downloaded to '${checksumPath}'"

    # Verify the content of the DLL
    if($PSCmdlet.ShouldProcess("${CSWDLLName} checksum")) {
        $source_checksum = (Get-Content -LiteralPath $checksumPath | ConvertFrom-Json)
        $checksum = Get-FileHash `
            -LiteralPath $dllPath `
            -Algorithm $source_checksum.Algorithm `
            -ErrorAction Stop
        if($source_checksum.Hash -ne $checksum.Hash) {
            Throw "'${CSWDLLName}' checksum did not match!"
        }
    }
    Write-Verbose "'${CSWDLLName}' checksum valid!"

    # Copy the DLL to system32
    $system32 = 'C:\Windows\SysWOW64'
    if($PSCmdlet.ShouldProcess("'${dllPath}' -> '${system32}'")) {
        Copy-Item `
            -LiteralPath $dllPath `
            -Destination "${system32}" `
            -ErrorAction Stop
    }
    Write-Verbose "'${dllPath}' copied to '${system32}'!"

    # Register the DLL
    $installedDll = "${system32}\${CSWDLLName}"
    if($PSCmdlet.ShouldProcess("Register '${installedDll}'")) {
        $p = Start-Process "${system32}\regsvr32.exe" `
            -ArgumentList "/s", "${installedDll}" `
            -Wait -NoNewWindow -PassThru
        if($p.ExitCode -ne "0") {
            Throw "Failed to register '${installedDll}'!"
        }
    }
    Write-Verbose "'${installedDll}' registered!"

    $files = @($checksumPath, $dllPath)
    foreach($f in $files) {
        Remove-TempFile `
            -File $f `
            -CleanupTempFiles $CleanupTempFiles
    }
}

function Remove-TempFile {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory=$true)]
        [Boolean]
        $CleanupTempFiles,
        [Parameter(Mandatory=$true)]
        [String]
        $File
    )

    if($CleanupTempFiles) {
        if($PSCmdlet.ShouldProcess($File)) {
            Remove-Item -LiteralPath $File
        }
        Write-Verbose "Cleanup - ${File}"
    }
}

function Install-CSW {
    [CmdletBinding(SupportsShouldProcess)]
    param (
        [Parameter(Mandatory=$true)]
        [String]
        $TempPath,
        [Parameter(Mandatory=$true)]
        [Boolean]
        $CleanupTempFiles,
        [Parameter(Mandatory=$true)]
        [String]
        $CSWInstaller,
        [Parameter(Mandatory=$true)]
        [String]
        $Server,
        # Username used to retrieve post-install updates directly from Connectship.
        [Parameter(Mandatory=$true)]
        [String]
        $MembersAreaUser,
        # Password used to retrieve post-install updates directly from Connectship.
        [Parameter(Mandatory=$true)]
        [String]
        $MembersAreaPassword,
        # Indicates whether updates should be installed immediately post-update 
        [Parameter(Mandatory=$true)]
        [Boolean]
        $PostInstallUpdates
    )

    $content = @"
[Options]
PostInstallUpdates=$(if($PostInstallUpdates) {"Y"} else {"N"})
MembersAreaUser=${MembersAreaUser}
MembersAreaPassword=${MembersAreaPassword}

[Products]
DeviceCubiscan=Y
DeviceDatamax=Y
DeviceDetecto=Y
DeviceFairbanks=Y
DeviceFlexWeigh=Y
DeviceGenericEpson=Y
DeviceGenericScanner=Y
DeviceGSE=Y
DeviceIntermec=Y
DeviceMettlerToledo=Y
DeviceMonarch=Y
DeviceNCI=Y
DevicePCL=Y
DevicePostScript=Y
DeviceOkidata=Y
DeviceQubeVu=Y
DeviceSamsung=Y
DeviceSato=Y
DeviceSetra=Y
DeviceStar=Y
DeviceToshiba=Y
DeviceUSBScale=Y
DeviceUSBScanner=Y
DeviceWindows=Y
DeviceZebra=Y

[CSW]
ServerURL=http://${server}
"@

    # Create temporary response file
    $responseFile = "${TempPath}\CSWInst.ini"
    if($PSCmdlet.ShouldProcess($responseFile)) {
        $content | Out-File -LiteralPath $responseFile
    }
    Write-Verbose "'${responseFile}' created!"

    # Run the installation silently
    $installer = "${TempPath}\${CSWInstaller}"
    if($PSCmdlet.ShouldProcess("${installer}")) {
        $p = Start-Process "${installer}" `
            -ArgumentList "/S", "/ACCEPTEULA=YES", "/RESPONSE=${responseFile}" `
            -Wait -NoNewWindow -PassThru
        if($p.ExitCode -ne "0") {
            Throw "Failed to install '${CSWInstaller}'!"
        }
    }
    Write-Verbose "'${installer}' installed!"
    
    $files = @($installer, $responseFile)
    foreach($f in $files) {
        Remove-TempFile `
            -File $f `
            -CleanupTempFiles $CleanupTempFiles
    }
}

# Configure default PS Actions
Set-StrictMode -Version Latest
$ErrorActionPreference = "Stop"
$PSDefaultParameterValues['*:ErrorAction']='Stop'

$Client = New-Object System.Net.WebClient

# Download the installer
Get-CSWInstaller `
    -Server $Server `
    -DownloadUrlPath $DownloadUrlPath `
    -TempPath $TempPath `
    -CSWInstaller $CSWInstaller `
    -Client $Client

# Download the utilites DLL and register it
Install-CSWDll `
    -Server $Server `
    -DownloadUrlPath $DownloadUrlPath `
    -CleanupTempFiles $CleanupTempFiles `
    -TempPath $TempPath `
    -CSWDLLName $CSWDLLName `
    -Client $Client

# Install-CSW
Install-CSW `
    -TempPath $TempPath `
    -CleanupTempFiles $CleanupTempFiles `
    -Server $Server `
    -CSWInstaller $CSWInstaller `
    -MembersAreaUser $MembersAreaUser `
    -MembersAreaPassword $MembersAreaPassword `
    -PostInstallUpdates $PostInstallUpdates
